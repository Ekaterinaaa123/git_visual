﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Конкатенация
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string FirstName = ("Имя = Ольга");
            string LastName = ("Фамилия = Петрушкина");
            string FullName = ("Полное имя Ольга Петрушкина");
            string s4 = (" ");
            Console.WriteLine(s4);
            string s5 = String.Concat(FirstName,", ", LastName, ", ", FullName );
            Console.WriteLine(s5);
        }
    }
}
